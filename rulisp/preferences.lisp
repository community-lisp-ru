;;; preferences.lisp

(in-package :restas.preferences)

(defpref :rulisp.baseurl "")

(defpref :rulisp.host "lisp.catap.ru:8080")

(defpref :rulisp.skin #P"skins/default")

(defpref :rulisp.content #P"content")